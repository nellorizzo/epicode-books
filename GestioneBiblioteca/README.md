Epicode Books
=============

> Gestione del catalogo di una biblioteca.

Specifiche
----------

La libreria gestisce nel catalogo libri e periodici.

Per ogni __libro__ è necessario gestire:
- Titolo
- Autori (possono essere più di uno)
- Editore
- Data di pubblicazione
- Codice ISBN (se presente)
- Codice interno della biblioteca
- Numero di copie a disposizione
- Volumi in cui è strutturato
- Lingua
- Categoria di classificazione (anche pi� di una)
- Numero di pagine totali (di tutti i volumi)

Ogni __copia__ a disposizione ha
- Una posizione nella biblioteca
- Una indicazione che comunica se disponibile per il prestito o meno
- Uno stato (in consultazione, in prestito, disponibile)

Ogni __Volume__ ha:
- Titolo
- Codice ISBN (se presente)
- Codice interno
- Numero di pagine

Ogni __Autore__ è caratterizzato da
- Nome
- Cognome
- Nazionalità
- Anno di nascita
- Anno di morte (se non in vita)

I __periodici__ sono caratterizzati da
- Data di pubblicazione
- Editore
- Titolo
- Categoria di classificazione (anche pi� di una)
- Periodicità (quotidiano, mensile, annuale, estemporanea, ecc.)

## Permessi
Gli **operatori** della biblioteca possono gestire il catalogo, mentre gli **utenti** possono solo fare delle ricerche.
